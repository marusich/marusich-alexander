package cucetestpackage.stepsdef;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import games.Matrix;
import org.junit.Assert;

import static cucetestpackage.run.TestRunner.matrix;

/**
 * Created by SANYA on 07.12.2016.
 */
public class MyStepdefsMatrix {
    public static int [][] result;

    @Given("^I start program Matrix$")
    public void IStartProgramMatrix() throws Throwable {
        matrix = new Matrix();
    }

    @When("^I use this method with value \"([^\"]*)\"$")
    public void iUseThisMethodWithValue(String Value) throws Throwable {
        result = Matrix.array(Integer.valueOf(Value));
    }

    @Then("^I check that array contains numbers from \"([^\"]*)\" to \"([^\"]*)\"$") //проверка что массив не содержит цифры менее 1 и более 9
    public void iCheckThatArrayContainsNumbersFromTo(String Value1, String Value2) throws Throwable {
        int z = Integer.valueOf(Value1);
        int s = Integer.valueOf(Value2);
        for (int[] x : result) {
            for (int y : x) {
                if (y < z || y > s) {
                    Assert.assertTrue("Массив содержит число меньше 1 или больше 9", false);
                }
            }
        }
    }
     @Then("^I check matrix in row \"([^\"]*)\" and position \"([^\"]*)\" with value \"([^\"]*)\"$") //проверка равенства введенного массива и полученного
     public void iCheckMatrixInRowAndPositionWithValue(String arg0, String arg1, String val3) throws Throwable {
         int z = Integer.valueOf(arg0);
         int s = Integer.valueOf(arg1);
         int w = Integer.valueOf(val3);
         Assert.assertTrue("Массивы не равны", result[z][s]==w);
      }
    @Then("^I check that Element Of Array \"([^\"]*)\" \"([^\"]*)\" equals \"([^\"]*)\"$") //проверка одного элемента массива
    public void iCheckThatElementOfArrayEquals(String Value1, String Value2, String Value3) throws Throwable {
        int z = Integer.valueOf(Value1);
        int s = Integer.valueOf(Value2);
        int q = Integer.valueOf(Value3);
        if (result [z][s]!=q)
            Assert.assertTrue("Элемент массива не равен 8", false);
    }

}


