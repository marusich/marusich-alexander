package cucetestpackage.stepsdef;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import figur.Rectangle;
import org.junit.Assert;

import static cucetestpackage.run.TestRunner.rectangle;

/**
 * Created by SANYA on 10.12.2016.
 */
public class MyStepdefsRectangle {
    static double a = 5;
    static double b = 5;
    static double c = 5;
    @Given("^I start program Rectangle$")
    public void IStartProgramRectangle() throws Throwable {
        rectangle = new Rectangle(a,b,c);
    }

    @Then("^I use method square rectangle and check that current value equals expected value \"([^\"]*)\"$") //сравнение ожидаемого значение и фактического
    public void iUseMethodSquareRectangleAndCheckThatCurrentValueEqualsExpectedValue(String value) throws Throwable {
        double z = Double.valueOf(value);
        double result =rectangle.square();
        Assert.assertEquals("Размер площади не совпадает с ожидаемым значением",z,result,0.0);
    }

    @When("^I use method replace rectangle with values \"([^\"]*)\" \"([^\"]*)\"$")
    public void iUseMethodReplaceRectangleWithValues(String val1, String val2) throws Throwable {
        double z = Double.valueOf(val1);
        double y = Double.valueOf(val2);
        rectangle.replace(z,y);
    }

    @Then("^I check new Coordinat rectangle doesn't equals Null$") //проверка на отображения пустых значений
    public void iCheckNewCoordinatRectangleDoesnTEqualsNull() throws Throwable {
        double[] getCoordinat = {rectangle.positionX, rectangle.positionY};
        Assert.assertNotNull("Новые значения пусты",getCoordinat);
    }

    @Then("^I am checking that current values equal expected values \"([^\"]*)\" \"([^\"]*)\"$") //проверка ожидаемого значения координат
    public void iAmCheckingThatCurrentValuesEqualExpectedValues(String arg1, String arg2) throws Throwable {
        double[] getCoordinat = {rectangle.positionX, rectangle.positionY};
        double z = Double.valueOf(arg1);
        double y = Double.valueOf(arg2);
        double [] expected = {z, y};
        Assert.assertArrayEquals("Значения новых координат не совпадают", expected, getCoordinat,0.0);
    }

    @Then("^I use method change rectangle with coefficient \"([^\"]*)\" and check size reduction$") //проверка уменьшения размера
    public void iUseMethodChangeRectangleWithCoefficientAndCheckSizeReduction(String arg0) throws Throwable {
        double w = Double.valueOf(arg0);
        double z = rectangle.change(w);
        if(z>c)
            Assert.assertTrue("Размер не уменьшились", false);
    }
}
