package cucetestpackage.stepsdef;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import games.Palindrom;
import org.junit.Assert;

import static cucetestpackage.run.TestRunner.palindrom;

/**
 * Created by SANYA on 09.12.2016.
 */
public class MyStepdefsPalindrom {
    @Given("^I start program Palindrom$")
    public void IStartProgramPalindrom() throws Throwable {
        palindrom = new Palindrom();
    }

    @Then("^I use method checkWord with word \"([^\"]*)\" and check on Palindrom$") // проверка "ротор" слова на палиндром
    public void iUseMethodCheckWordWithWordAndCheckOnPalindrom(String word1) throws Throwable {
        Assert.assertTrue("Слово является не палиндромом", palindrom.checkWord(word1));
    }

    @Then("^I use method checkWord with space in word \"([^\"]*)\" and check on Palindrom$") //проверка слово с пробелом на палиндром
    public void iUseMethodCheckWordWithSpaceInWordAndCheckOnPalindrom(String word2) throws Throwable {
        Assert.assertFalse("Слово является палиндромом", palindrom.checkWord(word2));
    }

    @Then("^I use method checkWord with same words \"([^\"]*)\" and check on Palindrom$") //проверка двух одинаковых слов на слово палиндром
    public void iUseMethodCheckWordWithSameWordsAndCheckOnPalindrom(String word3) throws Throwable {
        Assert.assertFalse("Слово является палиндромом", palindrom.checkWord(word3));
    }

    @Then("^I use method checkPhrase with same words \"([^\"]*)\" and check on Palindrom$")  //проверка фразы на ппалиндром
    public void iUseMethodCheckPhraseWithSameWordsAndCheckOnPalindrom(String phrase1) throws Throwable {
        Assert.assertTrue("Фраза является палиндромом", palindrom.checkPhrase(phrase1));
    }

    @Then("^I use method checkPhrase with Phrase two spaces \"([^\"]*)\" and check on Palindrom$") //проверка на отсекаемость пробелов в фразе
    public void iUseMethodCheckPhraseWithPhraseTwoSpacesAndCheckOnPalindrom(String phrase2) throws Throwable {
        Assert.assertTrue("Фраза не является  палиндромом", palindrom.checkPhrase(phrase2));
    }
}
