package cucetestpackage.stepsdef;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import games.Snail;
import org.junit.Assert;

import static cucetestpackage.run.TestRunner.snail;

/**
 * Created by SANYA on 09.12.2016.
 */
public class MyStepdefsSnail {
    public static int [][] result1;
    @Given("^I start program Snail$")
    public void IStartProgramSnail() throws Throwable {
        snail = new Snail();
    }

    @When("^I use calculateSnail method with \"([^\"]*)\" value$")
    public void iUseCalculateSnailMethodWithValue(String value) throws Throwable {
        result1 = Snail.calculateSnail(Integer.valueOf(value));
    }

    @Then("^I check that Max Value equals to \"([^\"]*)\"$") // проверка что самое большое число в массиве равно его размерности в квадрате
    public void iCheckThatMaxValueEqualsTo(String expectedValue) throws Throwable {
        int z = Integer.valueOf(expectedValue);
        int max = result1[0][0];
        for (int x = 0; x < result1.length; x++) {
            for (int y =0; y <result1.length;y++) {
                if(max<result1[x][y]) {
                    max = result1[x][y];
                }
            }
        }
        if (max != z) {
            Assert.assertFalse("самое большое число в массиве не равно его размерности в квадрате", true);
        }
    }

    @Then("^I check this matrix in row \"([^\"]*)\" and position \"([^\"]*)\" with value \"([^\"]*)\"$") //проверка равенства введенного масива и полученного
    public void iCheckThisMatrixInRowAndPositionWithValue(String arg0, String arg1, String arg2) throws Throwable {
        int z = Integer.valueOf(arg0);
        int s = Integer.valueOf(arg1);
        int w = Integer.valueOf(arg2);
        Assert.assertTrue("Массивы не равны", result1[z][s]==w);
    }

        @Then("^Checking that an Element Of Array \"([^\"]*)\" \"([^\"]*)\" equals \"([^\"]*)\"$") // проверка элемента массива
    public void checkingThatAnElementOfArrayEquals(String val1, String val2, String val3) throws Throwable {
        int ss = Integer.valueOf(val1);
        int tt = Integer.valueOf(val2);
        int qq = Integer.valueOf(val3);
        if (result1 [ss] [tt]!=qq)
            Assert.assertTrue("Элемент массива не равен 1", false);
    }

    @Then("^Checking that Element Of Array doesn't have \"([^\"]*)\" number$") // проверка на то что матрица не содержит ноль или отрицательные значения
    public void checkingThatElementOfArrayDoesnTHaveNumber(String value) throws Throwable {
        int z = Integer.valueOf(value);
        for (int [] x:result1){
            for (int y:x) {
                if (y <= z) {
                    Assert.assertTrue("Матрица содержит число меньше 0 или отрицательные значения", false);
                }
            }
        }
    }

}
