package cucetestpackage.stepsdef;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import figur.Circle;
import org.junit.Assert;

import static cucetestpackage.run.TestRunner.circle;

/**
 * Created by SANYA on 10.12.2016.
 */
public class MyStepsdefsCircle {
    static double a = 5;
    static double b = 5;
    static double c = 5;
    @Given("^I start program Circle$")
    public void IStartProgramCircle() throws Throwable {
        circle = new Circle(a,b,c);
    }


    @Then("^I use method square and check that current value equals expected value \"([^\"]*)\"$")
    public void iUseMethodSquareAndCheckThatCurrentValueEqualsExpectedValue(String value) throws Throwable {
        double z = Double.valueOf(value);
        double result =circle.square();
        Assert.assertEquals("Размер площади не совпадает с ожидаемым значением",z,result,0.0);
    }

    @When("^I use method with values \"([^\"]*)\" \"([^\"]*)\"$")
    public void iUseMethodWithValues(String val1, String val2) throws Throwable {
        double z = Double.valueOf(val1);
        double y = Double.valueOf(val2);
        circle.replace(z,y);
    }

    @Then("^I check new Coordinat doesn't equals Null$")
    public void iCheckNewCoordinatDoesnTEqualsNull() throws Throwable {
        double[] getCoordinat = {circle.positionX, circle.positionY};
        Assert.assertNotNull("Новые значения пусты",getCoordinat);
    }

    @Then("^I check that current values equal expected values \"([^\"]*)\" \"([^\"]*)\"$")
    public void iCheckThatCurrentValuesEqualExpectedValues(String arg1, String arg2) throws Throwable {
        double[] getCoordinat = {circle.positionX, circle.positionY};
        double z = Double.valueOf(arg1);
        double y = Double.valueOf(arg2);
        double [] expected = {z, y};
        Assert.assertArrayEquals("Значения новых координат не совпадают", expected, getCoordinat,0.0);
    }

    @Then("^I use method change with coefficient \"([^\"]*)\" and check size reduction$")
    public void iUseMethodChangeWithCoefficientAndCheckSizeReduction(String arg0) throws Throwable {
        double w = Double.valueOf(arg0);
        double z = circle.change(w);
        if(z>c)
            Assert.assertTrue("Размер не уменьшилсь", false);
    }
}
