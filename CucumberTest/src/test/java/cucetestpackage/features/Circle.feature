@OOP
Feature: testing circle
  Background:
    Given I start program Circle

  @OOP @Nonstatic
  Scenario: 001 Checking Expected Value Of Square
    Then I use method square and check that current value equals expected value "78.53981633974483"
  @OOP @Nonstatic
  Scenario: 002 Checking Empty Value
    When I use method with values "6" "6"
    Then I check new Coordinat doesn't equals Null
  @OOP @Nonstatic
  Scenario: 003 Checking Expected Value Of New Coordinat
    When I use method with values "51" "5"
    Then I check that current values equal expected values "51" "5"
  @OOP @Nonstatic
  Scenario: 004 Checking Of Size Reduction
    Then I use method change with coefficient "0.5" and check size reduction
