@Matrix
Feature: testing matrix
  Background:
    Given I start program Matrix

  @Matrix
  Scenario: 001 Checking That Array Contains Numbers From One To Nine
    When I use this method with value "4"
    Then I check that array contains numbers from "1" to "9"

  @Matrix
  Scenario Outline: 002 Checking That Arrays Are Equal
    When I use this method with value "3"
    Then  I check matrix in row "<row>" and position "<numberInRow>" with value "<value>"
    Examples:
      | row|numberInRow|value|
      | 0  |0          |1    |
    |0   |1          |2    |
    |0   |2          |3    |
    |1   |0          |4    |
    |1   |1          |5    |
    |1   |2          |6    |
    |2   |0          |7    |
    |2   |1          |8    |
    |2   |2          |9    |
  @Matrix
  Scenario: 003 Checking Element Of Array
    When I use this method with value "4"
    Then I check that Element Of Array "1" "3" equals "8"

