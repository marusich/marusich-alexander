@OOP
Feature: testing rectangle
  Background:
    Given I start program Rectangle

  @OOP @Nonstatic
  Scenario: 001 Checking Expected Value Of Square
    Then I use method square rectangle and check that current value equals expected value "25"
  @OOP @Nonstatic
  Scenario: 002 Checking Empty Value
    When I use method replace rectangle with values "6" "6"
    Then I check new Coordinat rectangle doesn't equals Null
  @OOP @Nonstatic
  Scenario: 003 Checking Expected Value Of New Coordinat
    When I use method replace rectangle with values "25" "6"
    Then I am checking that current values equal expected values "25" "6"
  @OOP @Nonstatic
  Scenario: 004 Checking Of Size Reduction
    Then I use method change rectangle with coefficient "0.5" and check size reduction