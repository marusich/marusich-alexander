@Palindrom
Feature: testing palindrom
  Background:
    Given I start program Palindrom

  @Palindrom @Nonstatic
  Scenario: 001 CheckingWordAsPalindrom
    Then I use method checkWord with word "ротор" and check on Palindrom
  @Palindrom @Nonstatic
  Scenario: 002 CheckingSpaceInWord
    Then I use method checkWord with space in word "ротор " and check on Palindrom
  @Palindrom @Nonstatic
  Scenario: 003 CheckingTheSameWordsWithSpaces
    Then I use method checkWord with same words "ротор ротор" and check on Palindrom
  @Palindrom @Nonstatic
  Scenario: 004 CheckingPhraseAsPalindrom
    Then I use method checkPhrase with same words "а роза упала на лапу азора" and check on Palindrom
  @Palindrom @Nonstatic
  Scenario: 005 CheckingDeletingSpacesInPhrase
    Then I use method checkPhrase with Phrase two spaces "а роза упала на лапу азора" and check on Palindrom

