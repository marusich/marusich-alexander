@Snail
Feature: testing snail
  Background:
    Given I start program Snail

  @SnailSpecial
  Scenario Outline: 001 Checking Max Value Of Snail
    When I use calculateSnail method with "<value>" value
    Then I check that Max Value equals to "<expectedValue>"
    Examples:
    | value | expectedValue |
    | 4     |  16           |
    | 10    | 100           |
    |20     |400            |
  @Snail
  Scenario Outline: 002 Checking That Arrays Are Equals
    When I use calculateSnail method with "3" value
    Then I check this matrix in row "<row>" and position "<numberInRow>" with value "<value>"
    Examples:
      | row|numberInRow|value|
      | 0  |0          |7    |
      |0   |1          |8    |
      |0   |2          |9    |
      |1   |0          |6    |
      |1   |1          |1    |
      |1   |2          |2    |
      |2   |0          |5    |
      |2   |1          |4    |
      |2   |2          |3    |
  @Snail
  Scenario: 003 Checking Element Of Snail
    When I use calculateSnail method with "3" value
    Then Checking that an Element Of Array "1" "1" equals "1"
  @Snail
  Scenario: 004 Checking Zero Or Negativ Digital
    When I use calculateSnail method with "4" value
    Then Checking that Element Of Array doesn't have "0" number
