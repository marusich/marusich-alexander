package cucetestpackage.run;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import figur.Circle;
import figur.Shape;
import games.Matrix;
import games.Palindrom;
import games.Snail;
import org.junit.runner.RunWith;

/**
 * Created by SANYA on 07.12.2016.
 */
@RunWith(Cucumber.class)

@CucumberOptions(
        features = "src/test/java/cucetestpackage/features",
        glue = "cucetestpackage/stepsdef",
        tags = "@Matrix,@Palindrom,@Snail,@SnailSpecial,@OOP,@Nonstatic"
)
public class TestRunner {
    public static Matrix matrix;
    public static Palindrom palindrom;
    public static Snail snail;
    public static Shape circle;
    public static Shape rectangle;
}
