package games;

public class Palindrom {
    public Boolean checkWord(String someWord) {
        someWord = someWord.toLowerCase();
        for (int i = 0; i < someWord.length() / 2; i++) {
            if (someWord.charAt(i) != someWord.charAt(someWord.length() - i - 1)) {
                System.out.println("Слово не явлется палиндромом");
                return false;
            }
        }
        System.out.println("Слово является палиндромом");
        return true;
    }
    public Boolean checkPhrase(String somePhrase){
        somePhrase = somePhrase.toLowerCase();
        somePhrase = somePhrase.replaceAll("\\W","");
        for (int i = 0; i < somePhrase.length() / 2; i++) {
            if (somePhrase.charAt(i) != somePhrase.charAt(somePhrase.length() - i - 1)) {
                System.out.println("Строка не является палиндромом");
                return false;
            }
        }
        System.out.println("Строка является палиндромом");
        return true;
    }
}


