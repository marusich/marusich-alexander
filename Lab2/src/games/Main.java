package games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {

            while (true) {
                try {
                    System.out.println("Enter some numbers from 1 to 9 :");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String n = reader.readLine();
                    int a = Integer.parseInt(n);
                    if (a >= 1 && a <= 9) {
                        int [] [] res = Matrix.Array(a);
                        for (int i = 0; i < res.length; i++) {
                            for (int j = 0; j < res[i].length; j++){
                             System.out.print(res[i][j]);
                            if (j < res[i].length - 1)
                            { System.out.print(" ");
                            }
                            } System.out.println();
                        }
                    } else {
                        System.out.println("Число заданно неверно!");
                        continue;
                    }
                    System.out.println("Если Вы хотите выйти введите слово exit. Что бы повторить нажите любую другую клавишу");
                    String x = reader.readLine();
                    if (x.equals("exit")) {
                        System.out.println("До встречи!");
                        break;
                    }

                } catch (NumberFormatException e) {
                    System.out.println("Введите пожалуйста число!!!");
                }
            }
        Snail test = new Snail();
        System.out.println("-----Построение двумерного массива в виде улитки-----");
        System.out.println("Введите значение равное 3 и более:");
        while(true) {
            BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
            String r = reader1.readLine();
            int vb = Integer.parseInt(r);
            if(vb<3){
                System.out.println("Значение введено меньше 3 !!! Повторите ввод");
            }else{
                test.calculateSnail(vb);
                break;
            }
        }
        System.out.println("------Проверка на Палиндром------");
        System.out.println("Введите любое слово или строку:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String n = reader.readLine();
        Palindrom test1 = new Palindrom();
        if(n.contains(" ")) {
            test1.checkPhrase(n);
        } else{
            test1.checkWord(n);
        }
    }
    }


