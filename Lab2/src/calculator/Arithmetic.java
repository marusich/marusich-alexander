package calculator;

import java.util.ArrayList;

public class Arithmetic {
    public double arrayMultiplication(ArrayList<Double> list) {
        int i;
        double mul = 1.0;
        for (i = 0; i < list.size(); i++) {
            mul *= list.get(i);
        }
        return mul;
    }

    public double power(double x, double y) {
        return Math.pow(x, y);
    }

    public double division(double x, double y) {
        return x /y+0.0;
    }

    public double root(double x) {
        return Math.sqrt(x);
    }
}

