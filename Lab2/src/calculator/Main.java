package calculator;

import games.Palindrom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        Arithmetic arr = new Arithmetic();
        ArrayList<Double> list = new ArrayList<>();
        System.out.println("------Умножение массива------");
        while (true) {
            System.out.println("Введите любое число:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String n = reader.readLine();
            if (n.isEmpty()) {
                System.out.println("результат умножения массива равен:");
                double res = arr.arrayMultiplication(list);
                System.out.println(res);
                break;
            } else {
                double a = Double.parseDouble(n);
                list.add(a);
                }
            }
        System.out.println("------Вычисление степени------");
        System.out.println("Введите  число");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String n = reader.readLine();
        double a = Double.parseDouble(n);
        System.out.println("Введите степень");
        String f = reader.readLine();
        double g = Double.parseDouble(f);
        System.out.println("Ваше число " + a + " в степени " + g + " равно:");
        double lx = arr.power(a,g);
        System.out.println(lx);

        System.out.println("------Деление чисел------");
        System.out.println("Введите первое число:");
        String d = reader.readLine();
        double q = Double.parseDouble(d);
        System.out.println("Введите второе число:");
        while(true){
            String m = reader.readLine();
            double l = Double.parseDouble(m);
            if(l==0){
                System.out.println("Деление на ноль!!! Введите новое число");
            }else{
                System.out.println("Результат деления равен:");
                double res = arr.division(q,l);
                System.out.println(res);
                break;
            }
        }
        System.out.println("------Вычисление квадратного корня------");
        while(true){
            String z = reader.readLine();
            double j = Double.parseDouble(z);
            if(j<=0){
                System.out.println("Введите число больше нуля!!!!");
            }else{
                System.out.println("Квадартный корень числа " + j +  "равен:");
                double res = arr.root(j);
                System.out.println(res);
                break;
            }
        }
        }

    }
