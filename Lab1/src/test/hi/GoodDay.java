package test.hi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import com.welcome.*;

public class GoodDay {
    public static void main(String[] args) throws IOException {
        Hello test = new Hello();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String n = reader.readLine();
        test.setupName(n);
        test.welcome();
        System.out.println("Hello world!!!");
        test.byeBay();
    }
}
