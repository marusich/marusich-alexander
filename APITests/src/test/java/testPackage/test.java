package testPackage;

import net.yandex.speller.services.spellservice.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/**
 * Created by Group1 on 1/23/2017.
 */
public class test {
    public static SpellService service;
    public static SpellServiceSoap port;
    public CheckTextRequest request;

    @BeforeClass
    public static void CreateService() {
        service = new SpellService();
        port = service.getSpellServiceSoap();
    }

    @Before
    public void prepareCleanRequest() {
        request = new CheckTextRequest();
    }

    @Test
    public void checkCorrectWord() {
        request.setLang("EN");
        request.setText("rowt houser resr");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Count of errors isn't three", checkTextResponse.getSpellResult().getError().size() == 3);
    }

    @Test
    public void checkPositionAndRow() {
        request.setLang("EN");
        request.setText("Water is cold in wwnter");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Position is incorrect ", checkTextResponse.getSpellResult().getError().get(0).getPos() == 17);
        Assert.assertTrue("Row is incorrect", checkTextResponse.getSpellResult().getError().get(0).getRow() == 0);
    }

    @Test
    public void checkPositionAndRowFromNewline() {
        request.setLang("EN");
        request.setText("Water\nfox\nwwnter");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("Position is incorrect ", checkTextResponse.getSpellResult().getError().get(0).getPos() == 10);
        Assert.assertTrue("Row is incorrect", checkTextResponse.getSpellResult().getError().get(0).getRow() == 2);
    }

    @Test
    public void checkRusWord() {
        request.setLang("RU");
        request.setText("Пьеса была сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("'сиграна' word isn't contains in the error list", checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("'сыграна' word isn't displayed in the correct list", checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));
    }
    @Test
    public void checkRusWordInCorrectList() {
        request.setLang("RU");
        request.setText("сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("'сиграна' word isn't contains in the error list", checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("Count of errors isn't five", checkTextResponse.getSpellResult().getError().get(0).getS().size()==5);
        Assert.assertTrue("'сыграна' word isn't displayed in the correct list", checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));
        }
    @Test
    public void checkRusWordWithoutSetLang() {
        request.setText("текстт");
        CheckTextResponse checkTextResponse = port.checkText(request);
        Assert.assertTrue("'сыграна' word isn't displayed in the correct list", checkTextResponse.getSpellResult().getError().get(0).getS().contains("текст"));
    }
    }
