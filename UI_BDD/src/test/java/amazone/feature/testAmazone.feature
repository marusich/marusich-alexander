@testamazone

Feature: testing Amazone

  Scenario: 001 checking Search page
      Given I enter "duck" word and press Submit button
      When I see result of search page

  Scenario: 002 checking count of result search
    Given I enter "duck" word and press Submit button
    When I see and compare first and second result

  Scenario: 003 checking adding items in the cart
    Given I enter "knife kitchen" word and press Submit button
    When I see result on  Knife page
    And I add to cart item
    And I enter "duck" word and press Submit button
    And I see result on  Duck page
    And I add to cart item
    Then I go to Cart page and see items in the carts

  Scenario: 004 checking deleting items in the cart
    Given I enter "book" word and press Submit button
    When I see result on  Book page
    And I add to cart item
    And I enter "marker" word and press Submit button
    And I see result on Marker page
    And I add to cart item
    And I enter "pen" word and press Submit button
    And I see result on Pen page
    And I add to cart item
    And I go to Cart page and see items in the cart
    Then I delete second item and check count of items

  Scenario: 005 checking deleting items in the cart
    Given I enter "pen" word and press Submit button
    When I see title item with first place and go to Bestsellers page
    Then I choose item on first place on the Bestsellers page  and compare them
