package amazone.run;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import pages.amazoneMapping.*;

@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/reportAmazone"},
        features = "src/test/java/amazone/feature",
        glue = "amazone/steps",
        tags = "@testamazone"
)
public class TestRunnerAmazone {
    public static WebDriver driver;
    public static MainPageAmazon mainPageAmazon;
    public static SearchPage searchPage;
    public static PageItem pageItem;
    public static Cart cart;
    public static PageBestsellers pageBestsellers;
}