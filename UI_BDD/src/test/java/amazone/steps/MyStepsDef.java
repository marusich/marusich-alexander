package amazone.steps;

import amazone.run.TestRunnerAmazone;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import org.openqa.selenium.firefox.FirefoxDriver;
import pages.amazoneMapping.*;

/**
 * Created by Irishka on 06.01.2017.
 */
public class MyStepsDef {
    String titleKnife;
    double priceKnife;
    String titleDuck;
    double priceDuck;
    String titleBook;
    String titleMarker;
    String titlePen;
    String title;

    @Before("@testamazone")
    public  static void  setUp(){
        TestRunnerAmazone.driver = new FirefoxDriver();
        TestRunnerAmazone.driver.manage().window().maximize();
        if(!TestRunnerAmazone.driver.getCurrentUrl().equals("https://www.amazon.com/"))
            TestRunnerAmazone.driver.get("https://www.amazon.com/");
        TestRunnerAmazone.mainPageAmazon = new MainPageAmazon(TestRunnerAmazone.driver);
        TestRunnerAmazone.searchPage = new SearchPage(TestRunnerAmazone.driver);
        TestRunnerAmazone.pageItem = new PageItem(TestRunnerAmazone.driver);
        TestRunnerAmazone.cart = new Cart(TestRunnerAmazone.driver);
        TestRunnerAmazone.pageBestsellers = new PageBestsellers(TestRunnerAmazone.driver);
    }

    @After("@testamazone")
    public  static void tearDown(){
        TestRunnerAmazone.driver.quit();
    }

    @Given("^I enter \"([^\"]*)\" word and press Submit button$")
    public void iEnterWordAndPressSubmitButton(String word) throws Throwable {
        TestRunnerAmazone.mainPageAmazon.searchField.sendKeys(word);
        TestRunnerAmazone.mainPageAmazon.navigateToSearchPage();
    }

    @When("^I see result of search page$")
    public void iSeeResultOfSearchPage() throws Throwable {
        Assert.assertTrue("Утка не отображается",TestRunnerAmazone.searchPage.resultDuck.isDisplayed());
    }

    @When("^I see and compare first and second result$")
    public void iSeeAndCompareFirstAndSecondResult() throws Throwable {
        String countFirst = TestRunnerAmazone.searchPage.count.getText().replaceAll("[^0-9]", " ").substring(4).replace(" ", "");
        int first = Integer.parseInt(countFirst);
        TestRunnerAmazone.searchPage.category.click();
        String countSecond = TestRunnerAmazone.searchPage.count.getText().replaceAll("[^0-9]", " ").substring(4).replace(" ", "");
        int second = Integer.parseInt(countSecond);
        Assert.assertTrue("Второй счетчик не уменьшился",first>second);
    }
    @When("^I see result on  Knife page$")
    public void iSeeResultOnKnifePage() throws Throwable {
        TestRunnerAmazone.searchPage.navigateToPageKnifes();
        titleKnife = TestRunnerAmazone.pageItem.title.getText();
        String knifePrice = TestRunnerAmazone.pageItem.price.getText().substring(1);
        priceKnife = Double.parseDouble(knifePrice);
    }

    @And("^I add to cart item$")
    public void iAddToCartItem() throws Throwable {
        TestRunnerAmazone.pageItem.addCart.click();
    }

    @And("^I see result on  Duck page$")
    public void iSeeResultOnDuckPage() throws Throwable {
        TestRunnerAmazone.searchPage.navigateToPageDucks();
        titleDuck = TestRunnerAmazone.pageItem.title.getText();
        String duckPrice =  TestRunnerAmazone.pageItem.price.getText().substring(1);
        priceDuck = Double.parseDouble(duckPrice);
    }

    @Then("^I go to Cart page and see items in the carts$")
    public void iGoToCartPageAndSeeItemsInTheCarts() throws Throwable {
        TestRunnerAmazone.pageItem.navigateToCart();
        String knifeTitleCart = TestRunnerAmazone.cart.titleKnife.getText();
        String duckTitleCart = TestRunnerAmazone.cart.titleDuck.getText();
        String priceTotal = TestRunnerAmazone.cart.totalPrice.getText().substring(1);
        double allPriceCart = Double.parseDouble(priceTotal);
        String countItems = TestRunnerAmazone.cart.items.getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItemsCount = Integer.parseInt(countItems);
        Assert.assertEquals("Название ножа не совпадают",titleKnife,knifeTitleCart);
        Assert.assertEquals("Название утки не совпадают",titleDuck,duckTitleCart);
        Assert.assertEquals("Общая цена не совпадает",allPriceCart,priceDuck+priceKnife,0.5);
        Assert.assertTrue("В корзине не два товара",cartItemsCount==2);
    }

    @When("^I see result on  Book page$")
    public void iSeeResultOnBookPage() throws Throwable {
        TestRunnerAmazone.searchPage.navigateToPageBooks();
        titleBook = TestRunnerAmazone.pageItem.title.getText();
    }

    @And("^I see result on Marker page$")
    public void iSeeResultOnMarkerPage() throws Throwable {
        TestRunnerAmazone.searchPage.navigateToPageMarkers();
        titleMarker = TestRunnerAmazone.pageItem.title.getText();
    }

    @And("^I see result on Pen page$")
    public void iSeeResultOnPenPage() throws Throwable {
        TestRunnerAmazone.searchPage.navigateToPagePens();
        titlePen = TestRunnerAmazone.pageItem.title.getText();
    }

    @And("^I go to Cart page and see items in the cart$")
    public void iGoToCartPageAndSeeItemsInTheCart() throws Throwable {
        TestRunnerAmazone.pageItem.navigateToCart();
        String cartBook = TestRunnerAmazone.cart.titleBooks.getText();
        String cartMarker = TestRunnerAmazone.cart.titleMarker.getText();
        String cartPen = TestRunnerAmazone.cart.titlePen.getText();
        Assert.assertEquals("Название книги не совпадает",titleBook,cartBook);
        Assert.assertEquals("Название маркера не совпадает",titleMarker,cartMarker);
        Assert.assertEquals("Название ручки не совпадает",titlePen,cartPen);
    }

    @Then("^I delete second item and check count of items$")
    public void iDeleteSecondItemAndCheckCountOfItems() throws Throwable {
        TestRunnerAmazone.cart.delete.get(1).click();
        TestRunnerAmazone.driver.navigate().refresh();
        String item = TestRunnerAmazone.cart.item.getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItems = Integer.parseInt(item);
        Assert.assertTrue("В корзине не два товара",cartItems==2);
    }

    @When("^I see title item with first place and go to Bestsellers page$")
    public void iSeeTitleItemWithFirstPlaceAndGoToBestsellersPage() throws Throwable {
        title = TestRunnerAmazone.searchPage.pensTitleBest.getText();
        TestRunnerAmazone.searchPage.navigateToPagePensBest();
        TestRunnerAmazone.pageItem.navigateToPageOfBestsellers();
    }

    @Then("^I choose item on first place on the Bestsellers page  and compare them$")
    public void iChooseItemOnFirstPlaceOnTheBestsellersPageAndCompareThem() throws Throwable {
        TestRunnerAmazone.pageBestsellers.navigateToPagePenBestseller();
        String title1place = TestRunnerAmazone.pageItem.title.getText();
        Assert.assertEquals("На первом месте отображается не выбранный предмет",title,title1place);
    }
}
