package stackOver.run;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.stackoverflowMapping.LoginPage;
import pages.stackoverflowMapping.MainPageStack;
import pages.stackoverflowMapping.SignUp;
import pages.stackoverflowMapping.TopQuestions;

import java.util.concurrent.TimeUnit;


@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/reportStack"},
        features = "src/test/java/stackOver/features",
        glue = "stackOver/steps",
        tags = "@teststackover"
)
public class TestRunner {
    public static WebDriver driver;
    public static MainPageStack mainPageStack;
    public static SignUp signUp;
    public static TopQuestions topQuestions;
    public static LoginPage loginPage;


    @BeforeClass
    public  static void  setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if(!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            driver.get("http://stackoverflow.com/");
        mainPageStack = new MainPageStack(driver);
        signUp = new SignUp(driver);
        topQuestions = new TopQuestions(driver);
        loginPage = new LoginPage(driver);
    }
    @AfterClass
    public  static void tearDown(){
        driver.quit();
    }
}