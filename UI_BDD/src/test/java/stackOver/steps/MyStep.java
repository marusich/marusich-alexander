package stackOver.steps;

import cucumber.api.java.en.And;
import org.junit.Assert;
import stackOver.run.TestRunner;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyStep {
    @Given("^I on stack start page$")
    public void iOnRozetkaStartPage() throws Throwable {
        Assert.assertTrue("Start page isn't displayed", TestRunner.mainPageStack.logo_stack.isDisplayed());
    }

    @Then("^checking Feaature$")
    public void checkingFeaature() throws Throwable {
        Assert.assertTrue("Fea_less_than_300",TestRunner.mainPageStack.fea_less_than_300.isDisplayed());
    }

    @When("^I click link SingUp$")
    public void iClickLinkSingUp() throws Throwable {
        TestRunner.mainPageStack.navigateToSignUp();
    }

    @Then("^I see Google and Facebook$")
    public void iSeeGoogleAndFacebook() throws Throwable {
        Assert.assertTrue("Google isn't displayed", TestRunner.signUp.google.isDisplayed() );
        Assert.assertTrue("Facebook isn't displayed", TestRunner.signUp.facebook.isDisplayed());
    }
    @And("^I click on Logo$")
    public void iClickOnLogo() throws Throwable {
         TestRunner.mainPageStack.logo_stack.click();
    }
    
    @When("^I click link Question$")
    public void iClickLinkQuestion() throws Throwable {
        TestRunner.mainPageStack.navigateToQa();
    }

    @Then("^I see data is today$")
    public void iSeeDataIs() throws Throwable {
        Assert.assertTrue("Data isn't today",TestRunner.topQuestions.nowDay.isDisplayed());
    }

    @Then("^I check preposition of price is not less than (\\d+) k$")
    public void iCheckPrepositionOfPriceIsNotLessThanK(int count) throws Throwable {
        Pattern pat = Pattern.compile("[-]?[0-9]+(.[0-9]+)?");
        String account = TestRunner.mainPageStack.money.getText();
        Matcher matcher = pat.matcher(account);
        while (matcher.find()) {
            int t = Integer.parseInt(matcher.group());
            Assert.assertTrue("Цена меньше чем 60 ", t > count);
        }
    }

    @When("^I move to SignUp page$")
    public void iMoveToSignUpPage() throws Throwable {
        TestRunner.mainPageStack.navigateToSignUp();
        Assert.assertTrue("URl doesn't", TestRunner.driver.getCurrentUrl().contains("stackoverflow"));
    }

    @And("^I enter credential data and login$")
    public void iEnterCredentialDataAndLogin() throws Throwable {
        TestRunner.signUp.Email.sendKeys("alextest47@yandex.ru");
        TestRunner.signUp.Pass.sendKeys("123654qw");
        TestRunner.signUp.Submit.click();

    }
    @Then("^I move to Login Page and see user logo$")
    public void iMoveToLoginPageAndSeeUserLogo() throws Throwable {
        TestRunner.signUp.navigateToLoginPage();
        String user = TestRunner.loginPage.profile.getText();
        Assert.assertEquals("Пользователь Alex не отображается на сайте", "Alex",user);

    }
}

