package pages.amazoneMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 23.12.2016.
 */
public class SearchPage {
    private WebDriver driver;

    public SearchPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//a[h2]//*[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), 'duck')]")
    public WebElement resultDuck;
    @FindBy(xpath = "//*[(@id = \"s-result-count\")]")
    public WebElement count;
    @FindBy(xpath = ".//*[@class=\"acs-mn2-navCell cell2 s-position-relative\"]")
    public WebElement category;
    @FindBy(xpath = ".//a[@title]//*[contains(text(),\"8 Inch\")]")
    public WebElement result8Inch;
    @FindBy(xpath = ".//a[h2]")
    public WebElement ducks;
    @FindBy(xpath = ".//a[@title]//*[contains(text(),\"Original\")]")
    public WebElement books;
    @FindBy(xpath = ".//a[@title]//*[contains(text(),\"2-Pack\")]")
    public WebElement markers;
    @FindBy(xpath = ".//a[@title]//*[contains(text(),\"Collection\")]")
    public WebElement pens;
    @FindBy(xpath = ".//*[@class=\"aok-float-left sx-badge-rectangle sx-bestseller-color\"]/ancestor::div[@class=\"a-fixed-left-grid-col a-col-right\"]//*[h2]")
    public WebElement pensTitleBest;

    public PageItem navigateToPageKnifes() {
        result8Inch.click();
        return new PageItem(driver);
    }
    public PageItem navigateToPageDucks() {
        ducks.click();
        return new PageItem(driver);
    }
    public PageItem navigateToPageBooks() {
        books.click();
        return new PageItem(driver);
    }
    public PageItem navigateToPageMarkers() {
        markers.click();
        return new PageItem(driver);
    }
    public PageItem navigateToPagePens() {
        pens.click();
        return new PageItem(driver);
    }
    public PageItem navigateToPagePensBest() {
        pensTitleBest.click();
        return new PageItem(driver);
    }

}
