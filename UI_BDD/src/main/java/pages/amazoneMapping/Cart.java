package pages.amazoneMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by SANYA on 23.12.2016.
 */
public class Cart {
    private WebDriver driver;

    public Cart (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'8 Inch')]")
    public WebElement titleKnife;
    @FindBy(xpath = ".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'Duck')]")
    public WebElement titleDuck;
    @FindBy(xpath = ".//*[@class=\"a-color-price a-text-bold\"]")
    public WebElement totalPrice;
    @FindBy(xpath = ".//*[@class=\"a-size-medium a-text-bold\"]")
    public WebElement items;
    @FindBy(xpath = ".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'Original')]")
    public WebElement titleBooks;
    @FindBy(xpath = ".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'2-Pack')]")
    public WebElement titleMarker;
    @FindBy(xpath = ".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'Collection')]")
    public WebElement titlePen;
    @FindBy(className = "sc-action-delete")
    public List<WebElement> delete;
    @FindBy(xpath = ".//*[@class=\"a-size-medium a-text-bold\"]")
    public WebElement item;

}
