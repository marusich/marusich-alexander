package pages.stackoverflowMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TopQuestions {

    private WebDriver driver;

    public TopQuestions(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//p[contains(@class,'label-key')]/b[text()='today']")
    public WebElement nowDay;

}

