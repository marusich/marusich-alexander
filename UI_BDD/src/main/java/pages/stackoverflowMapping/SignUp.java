package pages.stackoverflowMapping;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class SignUp {
    private WebDriver driver;

    public SignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//div[@class='text']/span[contains(text(),'Google')]")
    public WebElement google;
    @FindBy(xpath = ".//div[@class='text']/span[contains(text(),'Facebook')]")
    public WebElement facebook;
    @FindBy(xpath = ".//*[@id='email']")
    public WebElement Email;
    @FindBy(xpath = ".//*[@id='password']")
    public WebElement Pass;
    @FindBy(xpath = ".//*[@id='submit-button']")
    public WebElement Submit;
    @FindBy(xpath = ".//*[@class=\"gravatar-wrapper-24\"][contains(@title,\"Alex\")]")
    public WebElement Logo;

    public LoginPage navigateToLoginPage() {
        Logo.click();
        return new LoginPage(driver);
    }
}
