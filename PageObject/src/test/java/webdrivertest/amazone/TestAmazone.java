package webdrivertest.amazone;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.amazoneMapping.*;

/**
 * Created by SANYA on 25.12.2016.
 */
public class TestAmazone {

    private static WebDriver driver;
    private static MainPageAmazon mainPageAmazon;
    private SearchPage searchPage;
    private PageItem pageItem;
    private Cart cart;
    private PageBestsellers pageBestsellers;

    @Before
    public void  setUpBeforeAnyTest() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
        mainPageAmazon = new MainPageAmazon(driver);
    }
    @After
    public void tearDown() throws Exception{
        driver.close();
    }

    @Test
    public void Test1() {
        mainPageAmazon.searchField.sendKeys("duck");
        searchPage = mainPageAmazon.navigateToSearchPage();
        Assert.assertTrue("Утка не отображается",searchPage.resultDuck.isDisplayed());
    }
    @Test
    public void Test2() {
        mainPageAmazon.searchField.sendKeys("duck");
        searchPage = mainPageAmazon.navigateToSearchPage();
        String countFirst = searchPage.count.getText().replaceAll("[^0-9]", " ").substring(4).replace(" ", "");
        int first = Integer.parseInt(countFirst);
        searchPage.category.click();
        String countSecond = searchPage.count.getText().replaceAll("[^0-9]", " ").substring(4).replace(" ", "");
        int second = Integer.parseInt(countSecond);
        Assert.assertTrue("Второй счетчик не уменьшился",first>second);
    }
    @Test
    public void Test3() {
        mainPageAmazon.searchField.sendKeys("knife kitchen");
        searchPage = mainPageAmazon.navigateToSearchPage();
        pageItem = searchPage.navigateToPageKnifes();
        String titleKnife = pageItem.title.getText();
        String knifePrice =  pageItem.price.getText().substring(1);
        double priceKnife = Double.parseDouble(knifePrice);
        pageItem.addCart.click();
        mainPageAmazon.searchField.sendKeys("duck");
        mainPageAmazon.submit.click();
        searchPage = mainPageAmazon.navigateToSearchPage();
        pageItem = searchPage.navigateToPageDucks();
        String titleDuck = pageItem.title.getText();
        String duckPrice =  pageItem.price.getText().substring(1);
        double priceDuck = Double.parseDouble(duckPrice);
        pageItem.addCart.click();
        cart = pageItem.navigateToCart();
        String knifeTitleCart = cart.titleKnife.getText();
        String duckTitleCart = cart.titleDuck.getText();
        String priceTotal = cart.totalPrice.getText().substring(1);
        double allPriceCart = Double.parseDouble(priceTotal);
        String countItems = cart.items.getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItemsCount = Integer.parseInt(countItems);
        Assert.assertEquals("Название ножа не совпадают",titleKnife,knifeTitleCart);
        Assert.assertEquals("Название утки не совпадают",titleDuck,duckTitleCart);
        Assert.assertEquals("Общая цена не совпадает",allPriceCart,priceDuck+priceKnife,0.5);
        Assert.assertTrue("В корзине не два товара",cartItemsCount==2);
    }
    @Test
    public void Test4() {
        mainPageAmazon.searchField.sendKeys("book");
        searchPage = mainPageAmazon.navigateToSearchPage();
        pageItem = searchPage.navigateToPageBooks();
        String titleBook = pageItem.title.getText();
        pageItem.addCart.click();
        mainPageAmazon.searchField.sendKeys("marker");
        searchPage = mainPageAmazon.navigateToSearchPage();
        pageItem = searchPage.navigateToPageMarkers();
        String titleMarker = pageItem.title.getText();
        pageItem.addCart.click();
        mainPageAmazon.searchField.sendKeys("pen");
        mainPageAmazon.submit.click();
        searchPage = mainPageAmazon.navigateToSearchPage();
        pageItem = searchPage.navigateToPagePens();
        String titlePen = pageItem.title.getText();
        pageItem.addCart.click();
        cart = pageItem.navigateToCart();
        String cartBook = cart.titleBooks.getText();
        String cartMarker = cart.titleMarker.getText();
        String cartPen = cart.titlePen.getText();
        Assert.assertEquals("Название книги не совпадает",titleBook,cartBook);
        Assert.assertEquals("Название маркера не совпадает",titleMarker,cartMarker);
        Assert.assertEquals("Название ручки не совпадает",titlePen,cartPen);
        cart.delete.get(1).click();
        driver.navigate().refresh();
        String item = cart.item.getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItems = Integer.parseInt(item);
        Assert.assertTrue("В корзине не два товара",cartItems==2);
    }
    @Test
    public void Test5() {
        mainPageAmazon.searchField.sendKeys("pen");
        mainPageAmazon.submit.click();
        searchPage = mainPageAmazon.navigateToSearchPage();
        String title = searchPage.pensTitleBest.getText();
        pageItem = searchPage.navigateToPagePensBest();
        pageBestsellers = pageItem.navigateToPageOfBestsellers();
        pageItem = pageBestsellers.navigateToPagePenBestseller();
        String title1place = pageItem.title.getText();
        Assert.assertEquals("На первом месте отображается не выбранный предмет",title,title1place);
    }
}
