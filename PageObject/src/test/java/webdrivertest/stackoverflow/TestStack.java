package webdrivertest.stackoverflow;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.stackoverflowMapping.LoginPage;
import pages.stackoverflowMapping.MainPageStack;
import pages.stackoverflowMapping.SignUp;
import pages.stackoverflowMapping.TopQuestions;

import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestStack {

    private static WebDriver driver;
    private static MainPageStack mainPageStack;
    private SignUp signUp;
    private TopQuestions topQuestions;
    private LoginPage loginPage;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        mainPageStack = new MainPageStack(driver);
    }
    @Before
    public void Start() {
        driver.get("http://stackoverflow.com/");
    }
    @AfterClass
    public static void tearDown() {
        driver.close();
    }
    @Test
    public void CheckFeatured() {
        assertTrue("Fea_less_than_300",
                mainPageStack.Fea_less_than_300.isDisplayed());
    }
    @Test
    public void CheckSignUp() {
        signUp = mainPageStack.navigateToSignUp();
        signUp.checkURL();
        Assert.assertNotNull("Google_and_Facebook is displayed", signUp.Google_and_Facebook.isDisplayed());
    }
    @Test
    public void CheckData() {
        topQuestions = mainPageStack.navigateToQa();
        topQuestions.checkURL();
        assertTrue("Today is displayed", topQuestions.Data_Today.isDisplayed());
    }
    @Test
    public void CheckMoney() {
        Pattern pat = Pattern.compile("[-]?[0-9]+(.[0-9]+)?");
        String account = mainPageStack.money.getText();
        Matcher matcher = pat.matcher(account);
        while (matcher.find()) {
            int t = Integer.parseInt(matcher.group());
            Assert.assertTrue("Цена меньше чем 60 ", t > 60);
        }

    }
    @Test
    public void Login(){
        signUp = mainPageStack.navigateToSignUp();
        signUp.checkURL();
        signUp.Email.sendKeys("alextest47@yandex.ru");
        signUp.Pass.sendKeys("123654qw");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        signUp.Submit.click();
        loginPage = signUp.navigateToLoginPage();
        String user = loginPage.profile.getText();
        Assert.assertEquals("Пользователь Alex не отображается на сайте", "Alex",user);
    }
}