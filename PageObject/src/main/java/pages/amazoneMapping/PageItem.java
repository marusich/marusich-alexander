package pages.amazoneMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 26.12.2016.
 */
public class PageItem {
    private WebDriver driver;

    public PageItem(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='productTitle']")
    public WebElement title;
    @FindBy(xpath = ".//span[contains(@id,'priceblock')]")
    public WebElement price;
    @FindBy(xpath = ".//*[@id='add-to-cart-button']")
    public WebElement addCart;
    @FindBy(xpath = ".//*[@id='nav-cart']")
    public WebElement cart;
    @FindBy(xpath = ".//*[@class=\"badge-link\"]//*[@class=\"a-icon a-icon-addon p13n-best-seller-badge\"]")
    public WebElement bestsellers;

    public Cart navigateToCart() {
        cart.click();
        return new Cart(driver);
    }
    public PageBestsellers navigateToPageOfBestsellers() {
        bestsellers.click();
        return new PageBestsellers(driver);
    }
}
