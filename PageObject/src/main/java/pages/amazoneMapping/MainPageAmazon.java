package pages.amazoneMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 23.12.2016.
 */
public class MainPageAmazon {
    private WebDriver driver;

    public MainPageAmazon(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='twotabsearchtextbox']")
    public WebElement searchField;
    @FindBy(xpath = ".//*[@class='nav-input'][@type=\"submit\"]")
    public WebElement submit;

    public SearchPage navigateToSearchPage() {
        submit.click();
        return new SearchPage(driver);
    }

}
