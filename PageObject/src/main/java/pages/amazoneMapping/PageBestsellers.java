package pages.amazoneMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 26.12.2016.
 */
public class PageBestsellers {
    private WebDriver driver;

    public PageBestsellers (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@class=\"zg_itemImmersion\"]")
    public WebElement firstBestsellers;

    public PageItem navigateToPagePenBestseller() {
        firstBestsellers.click();
        return new PageItem(driver);
    }
}
