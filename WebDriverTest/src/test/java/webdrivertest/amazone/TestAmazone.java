package webdrivertest.amazone;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.List;
/**
 * Created by SANYA on 17.12.2016.
 */
public class TestAmazone {
    private static WebDriver driver;

    @Before
    public void  setUpBeforeAnyTest() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
    }
    @After
    public void tearDown() throws Exception{
        driver.close();
    }
    @Test
    public void Test1() throws Exception{
        String word = "duck";
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys(word);
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        List<WebElement> title = driver.findElements(By.cssSelector(".a-size-medium.a-color-null.s-inline.s-access-title.color-variation-title-replacement.a-text-normal"));
        for (WebElement titleOnSearchPage: title){
            String titleOnPage = titleOnSearchPage.getText().toLowerCase();
            Assert.assertTrue("Не все названия содержат 'duck' ", titleOnPage.contains(word));
        }
    }
    @Test
    public void Test2() throws Exception{
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("duck");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        String results = driver.findElement(By.xpath("//*[(@id = \"s-result-count\")]")).getText().replaceAll("[^0-9]", " ").substring(4).replace(" ", "");
        int first = Integer.parseInt(results);
        driver.findElement(By.xpath(".//*[@class=\"acs-mn2-navCell cell2 s-position-relative\"]")).click();
        String results1  = driver.findElement(By.xpath("//*[(@id = \"s-result-count\")]")).getText().replaceAll("[^0-9]", " ").substring(4).replaceAll(" ", "");
        int second = Integer.parseInt(results1);
        Assert.assertTrue("Второй счетчик не уменьшился",first>second);
}
    @Test
    public void Test3() throws Exception{
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("knife kitchen");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        driver.findElement(By.xpath(".//a[@title]//*[contains(text(),\"8 Inch\")]")).click();
        String titleKnife = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        String priceKn = driver.findElement(By.xpath(".//span[contains(@id,'priceblock')]")).getText().substring(1);
        double priceKnife = Double.parseDouble(priceKn);
        driver.findElement(By.xpath(".//*[@class=\"a-button-input\"][@type=\"submit\"]")).click();
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("duck");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        driver.findElement(By.xpath(".//a[h2]")).click();
        String titleDuck = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        String priceDu = driver.findElement(By.xpath(".//span[contains(@id,'priceblock')]")).getText().substring(1);
        Double priceDuck = Double.parseDouble(priceDu);
        driver.findElement(By.xpath(".//*[@class=\"a-button-input\"][@type=\"submit\"]")).click();
        driver.findElement(By.xpath(".//*[@id='nav-cart']")).click();
        String cartKnife = driver.findElement(By.xpath(".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'8 Inch')]")).getText();
        String cartDuck = driver.findElement(By.xpath(".//*[@class=\"a-size-medium sc-product-title a-text-bold\"][contains(text(),'Duck')]")).getText();
        String cart = driver.findElement(By.xpath(".//*[@class=\"a-color-price a-text-bold\"]")).getText().substring(1);
        double allPriceCart = Double.parseDouble(cart);
        String item = driver.findElement(By.xpath(".//*[@class=\"a-size-medium a-text-bold\"]")).getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItems = Integer.parseInt(item);
        Assert.assertEquals("Название ножа не совпадают",titleKnife,cartKnife);
        Assert.assertEquals("Название утки не совпадают",titleDuck,cartDuck);
        Assert.assertEquals("Общая цена не совпадает",allPriceCart,priceDuck+priceKnife,0.5);
        Assert.assertTrue("В корзине не два товара",cartItems==2);
    }
    @Test
    public void Test4() throws Exception{
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("book");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        List<WebElement> titleBooks = driver.findElements(By.cssSelector(".a-size-medium.a-color-null.s-inline.s-access-title.color-variation-title-replacement.a-text-normal"));
        titleBooks.get(0).click();
        String titleBook = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        driver.findElement(By.xpath(".//*[@id='add-to-cart-button']")).click();

        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("marker");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        List<WebElement> titleMarkers = driver.findElements(By.cssSelector(".a-size-medium.a-color-null.s-inline.s-access-title.color-variation-title-replacement.a-text-normal"));
        titleMarkers.get(0).click();
        String titleMarker = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        driver.findElement(By.xpath(".//*[@id='add-to-cart-button']")).click();

        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("pen");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        List<WebElement> titlePens = driver.findElements(By.cssSelector(".a-size-medium.a-color-null.s-inline.s-access-title.color-variation-title-replacement.a-text-normal"));
        titlePens.get(0).click();
        String titlePen = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        driver.findElement(By.xpath(".//*[@id='add-to-cart-button']")).click();

        driver.findElement(By.xpath(".//*[@id='nav-cart']")).click();
        List<WebElement> titleItems = driver.findElements(By.cssSelector(".a-size-medium.sc-product-title.a-text-bold"));
        for (WebElement titleOnCartPage: titleItems){
            String titleItem = titleOnCartPage.getText();
                Assert.assertTrue("Названия не совпадает", titleItem.equals(titleBook)||titleItem.equals(titleMarker)||titleItem.equals(titlePen));
        }
        List<WebElement> options= driver.findElements(By.className("sc-action-delete"));
        options.get(1).click();
        driver.navigate().refresh();
        String item = driver.findElement(By.xpath(".//*[@class=\"a-size-medium a-text-bold\"]")).getText().replaceAll("[^0-9]", " ").substring(1,12).replaceAll(" ", "");
        int cartItems = Integer.parseInt(item);
        Assert.assertTrue("В корзине не два товара",cartItems==2);
    }
    @Test
    public void Test5() throws Exception{
        // проверяем что выбранный BestSeller отображается в списке бестселлеров на первом месте
        driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).sendKeys("pen");
        driver.findElement(By.xpath(".//*[@class='nav-input'][@type=\"submit\"]")).click();
        String title = driver.findElement(By.xpath(".//*[@class=\"aok-float-left sx-badge-rectangle sx-bestseller-color\"]/ancestor::div[@class=\"a-fixed-left-grid-col a-col-right\"]//*[h2]")).getText(); // находим в списке бестселлер
        driver.findElement(By.xpath(".//*[@class=\"aok-float-left sx-badge-rectangle sx-bestseller-color\"]/ancestor::div[@class=\"a-fixed-left-grid-col a-col-right\"]//*[h2]")).click(); // переходим на эту книгу и запоминаем название
        driver.findElement(By.xpath(".//*[@class=\"badge-link\"]//*[@class=\"a-icon a-icon-addon p13n-best-seller-badge\"]")).click(); // переходим в список бестселлеров
        driver.findElement(By.xpath(".//*[@class=\"zg_itemImmersion\"]")).click(); // выбираем первый из списка
        String title1place = driver.findElement(By.xpath(".//*[@id='productTitle']")).getText();
        Assert.assertEquals("На первом месте отображается не выбранный предмет",title,title1place); // сравниваем
    }
}
