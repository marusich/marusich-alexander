package webdrivertest.stackoverflow;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SANYA on 19.12.2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;

    @BeforeClass
    public static void start() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    @Before
    public void  setUpBeforeAnyTest() throws Exception{
        driver.get("http://stackoverflow.com/");
    }
    @AfterClass
    public static void end() throws Exception{
        driver.close();
    }
    @After
    public void tearDown() throws Exception{
    }
    @Test
    public void Test1() throws Exception {
        String feature = driver.findElement(By.xpath("//span[@class='bounty-indicator-tab']")).getText();
        int feature1 = Integer.parseInt(feature);
        System.out.println(feature1);
        Assert.assertTrue("Рекомендуемых постов меньше чем 300", feature1>300);
    }

    @Test
    public void Test2() throws Exception {
        driver.findElement(By.xpath("//a[@class='login-link'][contains(text(),'sign')]")).click();
        Assert.assertTrue("кнопка Google отображается", driver.findElements(By.xpath(".//*[@class=\"text\"]/span[contains(text(),\"Google\")]")).size()>0);
        Assert.assertTrue("кнопка Facebook отображается", driver.findElements(By.xpath(".//*[@class=\"text\"]/span[contains(text(),\"Facebook\")]")).size()>0);
    }
    @Test
    public void Test3() throws Exception {
        driver.findElement(By.xpath("//a[@class='question-hyperlink'][1]")).click();
        Assert.assertTrue("Дата не сегодняшняя",driver.findElement(By.xpath(".//p[contains(@class,'label-key')]/b[text()='today']")).isDisplayed());
    }
    @Test
    public void Test4() throws Exception {
        Pattern pat = Pattern.compile("\\d+");
        List<WebElement> account = driver.findElements(By.xpath(".//div[contains(text(),'$')]"));
        if(account.isEmpty()){
            Assert.assertTrue("Нет цены на сайте",false);
        }else {
            for (int i = 0; i < account.size(); i++) {
                String test = account.get(i).getText();
                Matcher matcher = pat.matcher(test);
                while (matcher.find()) {
                    int t = Integer.parseInt(matcher.group());
                    Assert.assertTrue("Цена меньше чем 60 ", t > 60);
                }
            }
        }
        }
    @Test
    public void Test5() throws Exception {
        // Проверка логина пользователя на сайт
        driver.findElement(By.xpath(".//*[@class=\"login-link\"][2]")).click();
        driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("alextest47@yandex.ru");
        driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("123654qw");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//*[@id='submit-button']")).click();
        driver.findElement(By.xpath(".//*[@class=\"gravatar-wrapper-24\"][contains(@title,\"Alex\")]")).click();
        String profile = driver.findElement(By.xpath(".//*[@class=\"name\"][contains(text(),\"Alex\")]")).getText();
        Assert.assertEquals("Пользователь Alex не отображается на сайте", "Alex",profile);
    }
}
