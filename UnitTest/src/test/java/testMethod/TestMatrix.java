package testMethod;

import games.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 02.12.2016.
 */
public class TestMatrix {
    public static Matrix matrix;

    @BeforeClass
    public static void CreateMatrix(){
        matrix = new Matrix();
    }

    @Test
    public void CheckingThatArrayContainsNumbersFromOneToNine (){ //проверка что массив не содержит цифры менее 1 и более 9
        int [][] result = Matrix.array(4);
        for (int [] x:result){
                for (int y:x) {
                if (y <= 0 || y >= 10) {
                    Assert.assertTrue("Массив содержит число меньше 1 или больше 9", false);
                }
            }
        }
    }
    @Test
    public void CheckingThatArraysAreEqual (){ //проверка равенства введенного масива и полученного
        int [] [] result1 = Matrix.array(3);
        int [] [] expected = {{1,2,3},{4,5,6},{7,8,9}};
        Assert.assertArrayEquals("Массивы не равны",expected,result1);
    }
    @Test
    public void CheckingElementOfArray(){ //проверка одного элемента масива
        int [][] result2 = Matrix.array(4);
        if (result2 [1] [3]!=8)
                    Assert.assertTrue("Элемент массива не равен 8", false);
                }
}
