package testMethod;

import figur.Rectangle;
import figur.Shape;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 04.12.2016.
 */
public class TestRectangle {
    public static Shape rectangle;
    static double a = 5;
    static double b = 5;
    static double c = 5;

    @BeforeClass
    public static void CreateMatrix() {
        rectangle = new Rectangle(a, b, c);
    }
        @Test
        public void CheckingExpectedValueOfSquare(){ //сравнение ожидаемого значение и фактического
         double z =rectangle.square();
            Assert.assertEquals("Размер площади не совпадает с ожидаемым значением",25,z,0.0);
        }
    @Test
    public void CheckingEmptyValue() { //проверка на отображения пустых значений
        rectangle.replace(6,6);
        double[] getCoordinat = {rectangle.positionX, rectangle.positionY};
        Assert.assertNotNull("Новые значения пусты",getCoordinat);
    }
    @Test
    public void CheckingExpectedValueOfNewCoordinat() { //проверка ожидаемого значения координат
        rectangle.replace(25,6);
        double[] getCoordinat = {rectangle.positionX, rectangle.positionY};
        double [] expected = {25, 6};
        Assert.assertArrayEquals("Значения новых координат не совпадают", expected, getCoordinat,0.0);
    }
    @Test
    public void CheckingOfSizeReduction(){ //проверка уменьшения размера
        double z = rectangle.change(0.5);
        if(z>c)
        Assert.assertTrue("Размер не уменьшились", false);
    }
    }

