package testMethod;

import figur.Shape;
import figur.Triangle;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 04.12.2016.
 */
public class TestTriangle {
    public static Shape triangle;
    static double a = 5;
    static double b = 5;
    static double c = 5;

    @BeforeClass
    public static void CreateMatrix() {
        triangle = new Triangle(a, b, c);
    }
    @Test
    public void CheckingExpectedValueOfSquare(){ //сравнение ожидаемого значение и фактического
        double z =triangle.square();
        Assert.assertEquals("Размер площади не совпадает с ожидаемым значением",10.825317547305483,z,0.0);
    }
    @Test
    public void CheckingEmptyValue() { //проверка на отображения пустых значений
        triangle.replace(6,6);
        double[] getCoordinat = {triangle.positionX, triangle.positionY};
        Assert.assertNotNull("Новые значения пусты",getCoordinat);
    }
    @Test
    public void CheckingExpectedValueOfNewCoordinat() { //проверка ожидаемого значения координат
        triangle.replace(25,55);
        double[] getCoordinat = {triangle.positionX, triangle.positionY};
        double [] expected = {25, 55};
        Assert.assertArrayEquals("Значения новых координат не совпадают", expected, getCoordinat,0.0);
    }
    @Test
    public void CheckingOfSizeReduction(){ //проверка уменьшения размера
        double z = triangle.change(0.5);
        if(z>c)
            Assert.assertTrue("Размер не уменьшились", false);
    }
}

