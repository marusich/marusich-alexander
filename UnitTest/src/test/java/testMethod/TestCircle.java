package testMethod;

import figur.Circle;
import figur.Shape;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 04.12.2016.
 */
public class TestCircle {
    public static Shape circle;
    static double a = 5;
    static double b = 5;
    static double c = 5;

    @BeforeClass
    public static void CreateMatrix() {
        circle = new Circle(a, b, c);
    }
    @Test
    public void CheckingExpectedValueOfSquare(){ //сравнение ожидаемого значение и фактического
        double z =circle.square();
        Assert.assertEquals("Размер площади не совпадает с ожидаемым значением",78.53981633974483,z,0.0);
    }
    @Test
    public void CheckingEmptyValue() { //проверка на отображения пустых значений
        circle.replace(6,6);
        double[] getCoordinat = {circle.positionX, circle.positionY};
        Assert.assertNotNull("Новые значения пусты",getCoordinat);
    }
    @Test
    public void CheckingExpectedValueOfNewCoordinat() { //проверка ожидаемого значения координат
        circle.replace(51, 5);
        double[] getCoordinat = {circle.positionX, circle.positionY};
        double [] expected = {51, 5};
        Assert.assertArrayEquals("Значения новых координат не совпадают", expected, getCoordinat,0.0);
    }
    @Test
    public void CheckingOfSizeReduction(){ //проверка уменьшения размера
        double z = circle.change(0.5);
        if(z>c)
            Assert.assertTrue("Размер не уменьшились", false);
    }
}
