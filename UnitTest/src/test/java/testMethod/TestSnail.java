package testMethod;

import games.Snail;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 03.12.2016.
 */
public class TestSnail {
    public static Snail snail;

    @BeforeClass
    public static void CreateSnail(){
        snail=new Snail();
    }
    @Test
    public void CheckingMaxValueOfSnail() { // проверка что самое большое число в массиве равно его размерности в квадрате
        int m = 4;
        int[][] result = Snail.calculateSnail(m);
        int max = result[0][0];
        for (int x = 0; x < result.length; x++) {
            for (int y =0; y <result.length;y++) {
                if(max<result[x][y]) {
                    max = result[x][y];
                }
            }
        }
        //System.out.println(max);
        if (max != m * m) {
            Assert.assertFalse("самое большое число в массиве не равно его размерности в квадрате", true);
        }
    }
    @Test
    public void CheckingThatArraysAreEquals(){ //проверка равенства введенного масива и полученного
        int[][] result1 = Snail.calculateSnail(3);
        int [] [] expected = {{7,8,9},{6,1,2},{5,4,3}};
        Assert.assertArrayEquals("Массивы не равны",expected,result1);
    }
    @Test
    public void CheckingElementOfSnail(){ // проверка элемента массива
        int [][] result2 = Snail.calculateSnail(3);
        if (result2 [1] [1]!=1)
            Assert.assertTrue("Элемент массива не равен 1", false);
    }
    @Test
    public void CheckingZeroOrNegativDigital (){ // проверка на то что матрица не содержит ноль или оризательные значения
        int [][] result = Snail.calculateSnail(3);
        for (int [] x:result){
            for (int y:x) {
                if (y <= 0) {
                    Assert.assertTrue("Матрица содержит число меньше 0 или отрицательные значения", false);
                }
            }
        }
    }

}
