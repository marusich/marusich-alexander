package testMethod;

import games.Palindrom;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by SANYA on 03.12.2016.
 */
public class TestPalindrom {
    private static Palindrom palindrom;

    @BeforeClass
    public static void CreatePalindrom(){
        palindrom = new Palindrom();
    }

    @Test
    public void CheckingWordAsPalindrom (){ // проверка "ротор" слова на палиндром
        Assert.assertTrue("Слово является не палиндромом", palindrom.checkWord("ротор"));
    }
    @Test
    public void CheckingSpaceInWord (){ //проверка слово с пробелом на палиндром
        Assert.assertFalse("Слово является палиндромом", palindrom.checkWord("ротор "));
    }
    @Test
    public void CheckingTheSameWordsWithSpaces(){ //проверка двух одинаковых слов на слово палиндром
        Assert.assertFalse("Слово является палиндромом", palindrom.checkWord("ротор ротор"));
    }
    @Test
    public void CheckingPhraseAsPalindrom (){ //проверка фразы на ппалиндром
        Assert.assertTrue("Фраза является палиндромом", palindrom.checkPhrase("а роза упала на лапу азора"));
    }
    @Test
    public void CheckingDeletingSpacesInPhrase (){ //проверка на отсекаемость пробелов в фразе
        Assert.assertTrue("Фраза не является  палиндромом", palindrom.checkPhrase("а   роза упала   на   лапу азора"));
    }
}
